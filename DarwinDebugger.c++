
/**
 *  A simple debugging tool for use with the Darwin project in
 *  Glenn Downing's Object-Oriented Programming course (CS371p)
 *  at the University of Texas at Austin during Spring 2019.
 *
 *  Written by Vivian Cox on April 15, 2019 for personal use
 *  and distribution to other students in the class.
 */

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>
#include <fstream>

using namespace std;


//**********************************************************
//**********************************************************
/**
 *  To adapt this debugger to your darwin implementation, this
 *  is the section you must reimplement. Insert include statements
 *  for your code files, change the typedef 'simulator' to match
 *  the name of your Darwin class, and implement the functions
 *  init_grid, print_grid, step_grid, and add_creature.
 *  My implementations are included here to give you an idea of
 *  how these functions are supposed to work.
 *
 *  The structure of this debugger makes some minimal assumptions
 *  about how your darwin implementation is designed -- it is possible
 *  that further adaptations are necessary.
 */
//**********************************************************
//**********************************************************

// Include your Darwin code here
#include "Species.h"
#include "Creature.h"
#include "DarwinSimulator.h"
#include "Darwin.h"
#include "Darwin.c++"

// The type of the Darwin simulator object that holds
// information about the simulation grid
typedef DarwinSimulator simulator;

/**
 *  Initialize an empty grid of size rows x cols.
 *  sim is guaranteed non-null.
 *  *sim should be set to a pointer to the new simulator object.
 */
void init_grid (simulator** const sim, int rows, int cols) {
  delete *sim;

  // replace this line
  *sim = new simulator(rows, cols);
}

/**
 *  Print the current state of the grid.
 *  sim and *sim are both guaranteed non-null.
 */
void print_grid (simulator* const * const sim) {
  // replace this line
  (**sim).print(cout);
}

/**
 *  Advance the simulation by one step.
 *  sim and *sim are both guaranteed non-null.
 */
void step_grid (simulator* const * const sim) {
  // replace this line
  (**sim).step();
}

/**
 *  Add a creature to the simulation.
 *  No promises are made about the contents of input.
 *  However, we expect it to be in the same form as a standard line of
 *  darwin input: <creature type> <row> <col> <creature direction>.
 *  Your code should throw an exception if it is not in this form.
 *  sim and *sim are both guaranteed non-null.
 *
 *  Note: If your implementation of darwin does not allow creatures to
 *  be added once the simulation begins, you should check for that
 *  case here and print an error message.
 */
void add_creature (simulator* const * const sim, const string& input) {
  // replace these two lines
  istringstream sin("1\n" + input + "\n1 1\n");
  darwin_read(sin, **sim);
}

//**********************************************************
//**********************************************************

/* Debugger I/O implementation starts here */

// print help information
void show_help () {
  cout << "\t'i <rows> <columns>' - initialize a new grid" << endl;
  cout << "\t'c <type> <row> <column> <direction>' - add a creature" << endl;
  cout << "\t'f' <filename> - add creatures from file" << endl;
  cout << "\t<enter> - step the board one iteration" << endl;
  cout << "\t'h' - show this help menu" << endl;
  cout << "\t'e' - exit" << endl;
}


// process a one-line command from the user
// returns true iff the program should quit
bool process_command (const string& s, simulator** const sim) {

  // handle step command
  if (s.empty()) {
    if (*sim != nullptr) {
      step_grid(sim);
      print_grid(sim);
    } else {
      cout << "Initialize the grid before stepping." << endl;
    }

  // handle add creature command
  } else if (s[0] == 'c') {
    if (*sim != nullptr) {
      try {
        string t = s.substr(2); // remove the 'c' at beginning
        add_creature(sim, t);
      } catch (...) {
        cout << "Oops! Wrong input format!" << endl;
        return false;
      }
      cout << "Added a creature." << endl;
      print_grid(sim);
    } else {
      cout << "Initialize the grid before adding a creature." << endl;
    }

  // handle add creature from file command
  } else if (s[0] == 'f') {
    if (*sim != nullptr) {
      string line;
      string filename = s.substr(2);
      ifstream f;
      try {
        f.open(filename);
      } catch (...) {
        cout << "File does not exist: " << filename << endl;
        return false;
      }
      while (!getline(f, line).eof()) {
        try {
          add_creature (sim, line);
          cout << "Added a creature: " << line << endl;
        } catch (...) {
          cout << "Oops! improperly formatted line!" << endl;
          print_grid(sim);
          return false;
        }
      }
      cout << "Successfully added all creatures from file." << endl;
      print_grid(sim);
    } else {
      cout << "Initialize the grid before adding creatures." << endl;
    }

  // handle init command
  } else if (s[0] == 'i') {
    istringstream sin(s);
    char ignored;
    int rows;
    int cols;
    try {
      sin >> ignored >> rows >> cols;
      init_grid(sim, rows, cols);
    } catch (...) {
      cout << "Oops! Wrong input format!" << endl;
      return false;
    }
    cout << "Initialized board." << endl;
    print_grid(sim);

  // handle help command
  } else if (s[0] == 'h') {
    show_help();

  // handle exit command
  } else if (s[0] == 'e') {
    cout << "exiting." << endl;
    return true;

  // default case
  } else {
    cout << "Unrecognized command." << endl;
  }

  return false;
}


// main
int main () {
  cout << "*** Welcome to the Darwin Debugger ***" << endl;
  show_help();

  simulator* sim = nullptr;
  string s;

  while (true) {
    cout << "DD> ";
    getline(cin, s);
    if (process_command (s, &sim))
      break;
  }
}
